"use strict";
//APP_DATA_KEY is not longer use anymore
//const APP_DATA_KEY = "LocalStorage";
// If you want to get the data of login user use currentUser....
//for example: currentUser._name or currentUser._password

class Expenses
{
    constructor(name,amount,date, category){
        this._name=name;
        this._amount=amount;
        this._category=category;
        this._date= date;
    }

    get name()
    {
        return this._name;
    }

    get amount()
    {
        return this._amount;
    }

    get category()
    {
        return this._category;
    }

    get date()
    {
        return this._date;
    }

    fromData(expensesData)
    {
        this._name=expensesData._name;
        this._amount=expensesData._amount;
        this._category=expensesData._category;
        this._date= expensesData._date;
    }

}

var category=[
    "Transportation",
    "Food",
    "House",
    "Clothing",
    "Gift",
    "Saving",
    "other"];

class User
{
    constructor(inputName,inputPassword)
    {
        this._userName=inputName;
        this._income=0;
        this._balance=0;
        this._goal=[];
        this._password=inputPassword;
        this._list=[];
        this._incomeBudgeting=[];
    }

    get userName()
    {
        return this._userName;
    }

    get income()
    {
        return this._income;
    }

    get balance()
    {
        return this._balance;
    }

    get goal()
    {
        return this._goal;
    }
    get password()
    {
        return this._password;
    }
    
    get list()
    {
        return this._list;
    }
    get incomeBudgeting()
    {
        return this._incomeBudgeting;
    }
    addincomeBudgeting(targetCategory,amount)
    {
        let set = [targetCategory,amount];
        this._incomeBudgeting.push(set);
    }
    addExpenses(name,amount,date, category)
    {
        let spend = new Expenses(name,amount,date, category);
        this._list.push(spend);
    }

    addGoal(newGoal)
    {
        this._goal.push(newGoal);
    }
    totalExpenses()
    {
        var total=0;
        for(var i=0; i<this._list.length; i++ )
        {
            total+=Number( this._list[i]._amount);
        }
        return total;
    }
  // balance = income - expenses
    calBalance()
    {
        this.balance = this.income - totalExpenses();
    }

    fromData(userData)
    {
        this._userName=userData._userName;
        this._income=userData._income;
        this._balance=userData._balance;
        this._goal=userData._goal;
        this._password=userData._password;
        this._list=userData._list;
        this._incomeBudgeting=userData._incomeBudgeting;

        for (let i = 0; i< this._list.length; i++) {
            let expenses = new Expenses();
            expenses.fromData(this._list[i]);
        } 
    }
}

function localStorageCheckKey(key)
{
    let data = localStorage.getItem(key);
    if(data === null)
    {
        return false;
    }
    else
    {
        return true;
    }
    
}

function localStorageUpdateKey(key,data)
{
    data = JSON.stringify(data);
    localStorage.setItem(key, data);
}

function retrieve(key)
{
    let data = localStorage.getItem(key);
    try
    {
      data = JSON.parse(data);
    }
    catch (error)
        {
         alert(error);
     }
    finally
    {
      return data;
    }
}
const CURRENT_USER="USER";
//login page
// If you want to get the data of login user use currentUser....
const PREFIX = "budgting.app.";
function login()
{
    let loginNameRef = document.getElementById('username').value;
    let passwordRef = document.getElementById('password').value;

    let data = localStorage.getItem(PREFIX+loginNameRef);

    if(data==passwordRef)
    {
        localStorageUpdateKey(CURRENT_USER,[loginNameRef,passwordRef]);
        alert(`welcome ${loginNameRef}`);
        window.location="profile.html";
    }
}

function signUp()
{
    var user = document.getElementById("registerUsername").value;
    var pass1 = document.getElementById("registerPassword").value;
    var pass2 = document.getElementById("confirmPassword").value;
    if ((pass1 === pass2) && (pass1 !== ""))
    {
        if (localStorage)
        {
            var passwordToStore = pass1;

            localStorage.setItem(PREFIX + user, passwordToStore);
            let newUser = new User(user,passwordToStore);
            let checked = localStorageCheckKey(user);
            if (checked === true)
            {
                let data= retrieve(user);
                newUser.fromData(data);
            }
            else
            {
                localStorageUpdateKey(user,newUser);
            }
            var dialog = document.getElementById('signUp');  
            dialog.close();
            alert("Account created for username: " + user+"\r\n"+"Please login");
        }
    }
    else
    {
        alert("Passwords must match and cannot be empty.")
    }
}

function signOut()
{
    localStorageUpdateKey(CURRENT_USER,"notLogin");
}
let currentUserName = retrieve(CURRENT_USER);
let currentUser = new User(currentUserName[0],currentUserName[1]);
let data1 = retrieve(currentUserName[0]);
currentUser.fromData(data1);

let tdy = new Date().toLocaleDateString;