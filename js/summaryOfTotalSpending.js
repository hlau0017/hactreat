"use strict";
/*
anychart.onDocumentReady(function() {
 
    // set the data
    var data = {
        header: ["Category", "Money Remaining"],
        rows: [
          ["Transportation", 1500],
          ["Food", 87000],
          ["Housing", 175000],
          ["Clothing", 10000],
          ["Saving", 242000],
          ["Gift", 25000],
          ["Other", 50000]
    ]};

    // create the chart
    var chart = anychart.column();

    // add the data
    chart.data(data);

    // set the chart title
    chart.title("Amount of Money Remaining for each Category");

    // draw
    chart.container("container");
    chart.draw();
  });

let a = retrieve(APP_DATA_KEY); // restrive data from local storage, that has been added in the input.html page

let b = 0; // variable into which numbers will be added, can be seen in the next "class", might not work, need to think of potentcially another solution

class random
{
  addition(input)
{
  let b = b+input;
  return b;
}
}
/*
transportationTotal = new random();
foodTotal = new random();
housingTotal = new random();
clothingTotal = new random();
savingTotal = new random();
giftTotal = new random();
otherTotal = new random();
 
function graphData(input)
{
  if(a.categoryRef = "Transportation")
  {
    let a = transportationTotal.addition();
  }

  else if (a.categoryRef = "Food")
    {
      let z = foodTotal.addition();
    }

    else if(a.categoryRef = "Housing")
    {
      let c = housingTotal.addition();
    }

    else if(a.categoryRef = "Clothing")
    {
      let d = clothingTotal.addition();
    }

    else if(a.categoryRef = "Saving")
    {
      let e = savingTotal.addition();
    }

    else if(a.categoryRef = "Gift")
    {
      let f = giftTotal.addition();
    }

    else if(a.categoryRef = "Other")
    {
      let g = otherTotal.addition();
    }
}


function balanceChart()
{
  // we need a card
}

function summaryChart() {
  // set the data
  var data = {
      header: ["Category", "Money Remaining"],
      rows: [
          ["Transportation", 1500],
          ["Food", 87000],
          ["Housing", 175000],
          ["Clothing", 10000],
          ["Saving", 242000],
          ["Gift", 25000],
          ["Other", 50000]
      ]
  };

  // create the chart
  var chart = anychart.column();

  // add the data
  chart.data(data);

  // set the chart title
  chart.title("Amount of Money Remaining for each Category");

  // draw
  chart.container("container");
  chart.draw();
}
*/
var yValues = [];
for(let i=0; i<category.length;i++)
{
  yValues.push(expensesByCategory(category[i]));
}


var barColors = [
  "#b91d47",
  "#00aba9",
  "#2b5797",
  "#e8c3b9",
  "#1e7145"
];

new Chart("myChart", {
  type: "doughnut",
  data: {
    labels: category,
    datasets: [{
      backgroundColor: barColors,
      data: yValues
    }]
  },
  options: {
    title: {
      display: true,
      text: "Monthly Expenses"
    }
  }
});
function expensesByCategory(targetCategory)
{
    let expense=0;
    for(let i=0; i<currentUser._list.length;i++){
      if(targetCategory==currentUser._list[i]._category)
      {
          expense += currentUser._list[i]._amount;
      }
    }
    return expense;
}

function getIncomeBudgeting(targetCategory)
{
  let budget =0;
  for(let i=0; i<currentUser._incomeBudgeting.length;i++)
  {
    if(targetCategory==currentUser._incomeBudgeting[i][0])
    {
        budget+=currentUser._incomeBudgeting[i][1];
    }
  }
}


let expensesDisplayRef = document.getElementById("expensesDisplay");
let incomeDisplayRef = document.getElementById("incomeDisplay");
let balanceDisplayRef = document.getElementById("balanceDisplay");
incomeDisplayRef.innerHTML=`$${currentUser._income}`;
expensesDisplayRef.innerHTML=`$${currentUser.totalExpenses()}`;
balanceDisplayRef.innerHTML =`$${currentUser._income-currentUser.totalExpenses()}`;

document.getElementById("transportProgress").value=calPercentage(category[0]);
//transportProgressRef.value=calPercentage(category[0]);
document.getElementById("foodProgress").value=calPercentage(category[1]);
document.getElementById("houseProgress").value=calPercentage(category[2]);
document.getElementById("clothingProgress").value=calPercentage(category[3]);
document.getElementById("giftProgress").value=calPercentage(category[4]);
document.getElementById("savingProgress").value=calPercentage(category[5]);
document.getElementById("otherProgress").value=calPercentage(category[6]);

function calPercentage(targetcategory)
{
  let percent=0;
  for(let i=0;i<currentUser._incomeBudgeting.length;i++){
    if(targetcategory==currentUser._incomeBudgeting[i][0])
    {
      percent=(expensesByCategory(targetcategory))/(currentUser._incomeBudgeting[i][1]);
    }
  }
  return percent;
}

function tdyExpenses()
{

  let display = document.getElementById("todayExpensesDetail");
  let content = "";
  let total=0;
  for(let i=0; i<currentUser._list.length;i++)
  {
    if(new Date(currentUser._list[i]._date).toLocaleDateString()==tdy)
    {
      content+=`                    
      <tr>
      <th>${currentUser._list[i]._category}</th>
      <td>${currentUser._list[i]._amount}</td>
      </tr>`
      total+=currentUser._list[i]._amount;
    }
  }
  display.innerHTML=content;
  document.getElementById("todayExpenses").value=total;
  
}
tdyExpenses();